import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<void> handleBackgroundMessage(RemoteMessage message) async{
}

class FirebaseAPI{
  final _firebaseMessaging=FirebaseMessaging.instance;

  final _androidChannel=const AndroidNotificationChannel(
    'high_importance_channel',
    'High Importance Notifications',
    description:"This channel is used for important notifications",
    importance: Importance.defaultImportance,
  );
  final _localNotifications=FlutterLocalNotificationsPlugin();

  Future initLocalNotifications() async{
    const iOS = DarwinInitializationSettings();
    const android = AndroidInitializationSettings('@drawable/ic_capec');
    const settings = InitializationSettings(android: android, iOS:iOS);

    await _localNotifications.initialize(
        settings
    );
    final platform=_localNotifications.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>();
    await platform?.createNotificationChannel(_androidChannel);
  }
  Future<void> initNotifications() async{
    await _firebaseMessaging.requestPermission();
    FirebaseMessaging.instance.subscribeToTopic('capec');
    FirebaseMessaging.onBackgroundMessage(handleBackgroundMessage);
    FirebaseMessaging.onMessage.listen((message) {
      final notification=message.notification;
      if (notification == null) return;
      _localNotifications.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            _androidChannel.id,
            _androidChannel.name,
            channelDescription: _androidChannel.description,
            icon: '@drawable/ic_capec',
          ),
        ),
        payload: jsonEncode(message.toMap()),
      );
    });
    initLocalNotifications();
  }
}