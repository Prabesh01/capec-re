import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:background_downloader/background_downloader.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:volume_controller/volume_controller.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class WebViewStack extends StatefulWidget {
  const WebViewStack({super.key});

  @override
  State<WebViewStack> createState() => _WebViewStackState();
}

class _WebViewStackState extends State<WebViewStack>{
  final GlobalKey webViewKey = GlobalKey();

  DownloadTask? backgroundDownloadTask;
  final RegExp regex = RegExp(r'^https:\/\/app\.cote\.ws\/.{4}\/$');
  bool docpage = false;
  double _lastvolume=0;
  double _getVolume =0;

  InAppWebViewController? webViewController;
  InAppWebViewSettings settings = InAppWebViewSettings(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: false,
      allowsInlineMediaPlayback: true,
      iframeAllowFullscreen: true,
    allowsBackForwardNavigationGestures: true,
  );
  DateTime? lastSavedTime=DateTime.now();
  PullToRefreshController? pullToRefreshController;
  double progress = 0;

  Future<void> _launchURL() async {
    webViewController?.loadUrl(
        urlRequest: URLRequest(url: WebUri("https://app.cote.ws/@capec/5s/")));
  }

  void _copyAndShowUrl() async {
    final currentUrl = await webViewController?.getUrl();

    await Clipboard.setData(ClipboardData(text: '$currentUrl'));

    Fluttertoast.showToast(
        msg: '$currentUrl',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: ['email','openid','profile'],
  );

  void _handleGoogleSignIn() async {
    try {
      GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      String accessToken = googleAuth.accessToken ?? '';
      final response = await http.post(
        Uri.parse('https://app.cote.ws/auth/login/'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'token': accessToken,
        }),
      );
      final loginCookie = response.headers['set-cookie'].toString();
      RegExp sessionIdRegex = RegExp(r"sessionid=([^;]+)");
      Match? sessionIdMatch = sessionIdRegex.firstMatch(loginCookie);
      String sessionId = 'NA';
      if (sessionIdMatch != null) {
        sessionId = sessionIdMatch.group(1)!;
      } else {
        Fluttertoast.showToast(
            msg: 'try again later',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0
        );
        return;
      }
      CookieManager cookieManager = CookieManager.instance();
      final url = WebUri("https://app.cote.ws/");

      await cookieManager.setCookie(
        url: url,
        name: "sessionid",
        value: sessionId
      );
      List<Cookie> cookies = await cookieManager.getCookies(url: url);
      await Clipboard.setData(ClipboardData(text: '$cookies'));
    } else {
      Fluttertoast.showToast(
          msg: 'canceled',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );
      return;
    }
    webViewController?.loadUrl(
        urlRequest: URLRequest(url: WebUri("https://app.cote.ws/@capec/5s/")));
    } catch (error) {
      Fluttertoast.showToast(
          msg: '$error',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }


  }

  void _logout() async {
    await googleSignIn.signOut();
    Fluttertoast.showToast(
      msg: 'Logged out successfully',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  @override
  void initState() {
    super.initState();

    VolumeController().showSystemUI = false;
    VolumeController().getVolume().then((volume) => _lastvolume = volume);

    pullToRefreshController = kIsWeb ? null : PullToRefreshController(
      settings: PullToRefreshSettings(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (defaultTargetPlatform == TargetPlatform.android) {
          webViewController?.reload();
        } else if (defaultTargetPlatform == TargetPlatform.iOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );

    FileDownloader()
        .configureNotificationForGroup(FileDownloader.defaultGroup,
        running: const TaskNotification(
            'Download {filename}', 'File: {filename} - {progress}'),
        complete: const TaskNotification(
            'Download {filename}', 'Download complete'),
        error: const TaskNotification(
            'Download {filename}', 'Download failed'),
        paused: const TaskNotification(
            'Download {filename}', 'Paused with metadata {metadata}'),
        progressBar: true)
        .configureNotification(
        complete: const TaskNotification(
            'Download {filename}', 'Download complete'),
    );

    VolumeController().listener((volume) {
      if(docpage) {
        if(DateTime.now().difference(lastSavedTime!).inSeconds<1){
          return;
        }
        lastSavedTime=DateTime.now();
        VolumeController().getVolume().then((volume) => _getVolume = volume);
        if(_getVolume < _lastvolume) {
          webViewController?.evaluateJavascript(source: "slidedown();");
        }else if (_getVolume > _lastvolume){
          webViewController?.evaluateJavascript(source: "slideup();");
        }else if (_getVolume==0){
          webViewController?.evaluateJavascript(source: "slidedown();");
        }else if (_getVolume==1){
          webViewController?.evaluateJavascript(source: "slideup();");
        }
        _lastvolume=_getVolume;
      }else {
        setState(() => _lastvolume = volume);
      }
    });

  }

  @override
  void dispose() {
    VolumeController().removeListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          final controller = webViewController;
          if (controller != null) {
            if (await controller.canGoBack()) {
              controller.goBack();
              return false;
            }
          }
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.grey[900],
            title: GestureDetector(
              onTap: () {
                webViewController?.reload();
              },onLongPress: () {
              _copyAndShowUrl();
            },
              child: const Text(
                'CAPEC',
                style: TextStyle(color: Colors.white),
              ),
            ),
            actions: [
              GestureDetector(
                onTap: _launchURL,
                onLongPress: () {
                  _copyAndShowUrl();
                },
                child: Image.asset(
                  'assets/ic_capec.png',
                  height: 50,
                  fit: BoxFit.contain,
                ),
              ),
            ],
          ),
          body: Column(children: <Widget>[Expanded(
    child:Stack(
            children: [
            InAppWebView(
            key: webViewKey,
            initialUrlRequest:
            URLRequest(url: WebUri("https://app.cote.ws/@capec/5s/")),
            initialSettings: settings,
            pullToRefreshController: pullToRefreshController,
            onWebViewCreated: (controller) {
              webViewController = controller;
            },
            onPermissionRequest: (controller, request) async {
              return PermissionResponse(
                  resources: request.resources,
                  action: PermissionResponseAction.GRANT);
            },
            shouldOverrideUrlLoading:
                (controller, navigationAction) async {
              var uri = navigationAction.request.url.toString();
              if (uri.contains('auth/login') || uri.contains('signin/oauth/error')) {
                _handleGoogleSignIn();
                return NavigationActionPolicy.CANCEL;
              }
              if (uri.contains('auth/logout/')) {
                _logout();
              }
              if (!uri.startsWith('https://app.cote.ws')) {
                  await launchUrl(
                      navigationAction.request.url!, mode: LaunchMode.externalApplication
                  );
                  return NavigationActionPolicy.CANCEL;
                }
              if(regex.hasMatch(uri)){
                docpage = true;
              }else{
                docpage = false;
              }
              if (uri.startsWith('https://app.cote.ws/uploads/') && uri.endsWith('.txt')) {
                await downloadFile(uri);
                return NavigationActionPolicy.DOWNLOAD;
              }

              return NavigationActionPolicy.ALLOW;
            },
              onDownloadStartRequest: (controller, downloadStartRequest) async {
                await downloadFile(downloadStartRequest.url.toString(),
                    downloadStartRequest.suggestedFilename);
              },
            onLoadStop: (controller, url) async {
              pullToRefreshController?.endRefreshing();
            },
            onReceivedError: (controller, request, error) {
              pullToRefreshController?.endRefreshing();
              controller.loadFile(assetFilePath: "assets/error.html");
            },
            onProgressChanged: (controller, progress) {
              if (progress == 100) {
                pullToRefreshController?.endRefreshing();
              }
              setState(() {
                this.progress = progress / 100;
              });
            },
          ),
          progress < 1.0
              ? LinearProgressIndicator(value: progress)
              : Container(),
          ]),
          ),
          ])  ),
    );
  }

  Future<void> downloadFile(String url, [String? filename]) async {
    var hasStoragePermission = await Permission.storage.isGranted;
    if (!hasStoragePermission) {
      final status = await Permission.storage.request();
      hasStoragePermission = status.isGranted;
    }
    if (hasStoragePermission) {
      final task = DownloadTask(
        url: url,
        filename: filename,
        updates: Updates.statusAndProgress,
      );
      //final result =
      await FileDownloader().download(task);
      // final newFilepath =
      await FileDownloader().moveToSharedStorage(task, SharedStorage.downloads);
    }else{
      Fluttertoast.showToast(
          msg: 'Storage permission denied!',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

}